﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileActorController : BaseActorController {

	/// <summary>
	/// the direction the projectile will move
	/// </summary>
	[SerializeField] Vector3 m_movementVector;

	[SerializeField] float m_destructionDistance = 10;

	override protected void ControlMovement()
	{
		///movement of the projectile into the direction
		this.transform.position += m_movementVector * Time.deltaTime;

		///remove this object if it is to far away
		if (Vector3.Distance (Vector3.zero, this.transform.position) > m_destructionDistance) {

			Destroy (this.gameObject);
		}
	}
}
