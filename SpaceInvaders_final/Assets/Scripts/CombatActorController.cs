﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatActorController : BaseActorController {

	/// <summary>
	/// reference to the bullet
	/// </summary>
	[SerializeField] GameObject m_projectilePrefab;

	/// <summary>
	/// offset of the bullet
	/// </summary>
	[SerializeField] Vector3 m_projectileOffset;

	public void FireProjectile()
	{
		GameObject.Instantiate (m_projectilePrefab, this.transform.position + m_projectileOffset, Quaternion.identity);
	}

	void OnCollisionEnter2D(Collision2D collider) 
	{
		TriggerDestruction ();

		Destroy (collider.gameObject);
	}

	virtual protected void TriggerDestruction()
	{
		Destroy (gameObject);
	}
}


