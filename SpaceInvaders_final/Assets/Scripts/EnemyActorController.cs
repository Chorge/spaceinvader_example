﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActorController : CombatActorController {
	
	[SerializeField] float m_movementSpeed = 1.0f;
	[SerializeField] float m_escaltionSpeed = 0.25f;
	[SerializeField] float m_movementEscalationDownMovement = 0.25f;
	[SerializeField] float m_maxMovementLimit = 1.0f;
	[SerializeField] bool m_switchMovement = false;

	Vector3 m_startPosition;

	float moveIntoDirection = 1.0f;

	void Start()
	{
		m_startPosition = transform.position;
	}

	override protected void ControlMovement()
	{
		Vector3 myPosition;

		if (m_switchMovement) 
		{
			moveIntoDirection += m_movementSpeed * Time.deltaTime;

			myPosition = this.transform.position;
			myPosition.x += m_movementSpeed * Time.deltaTime;

			if (myPosition.x > m_startPosition.x + m_maxMovementLimit) 
			{
				myPosition.x = m_startPosition.x + m_maxMovementLimit;
				m_switchMovement = false;
			}
		}
		else
		{
			moveIntoDirection -= m_movementSpeed * Time.deltaTime;

			myPosition = this.transform.position;
			myPosition.x -= m_movementSpeed * Time.deltaTime;

			if (myPosition.x < m_startPosition.x - m_maxMovementLimit) 
			{
				myPosition.x = m_startPosition.x - m_maxMovementLimit;
				m_switchMovement = true;
			}
		}	

		this.transform.position = myPosition; 
	}

	public void TriggerEscalate()
	{
		m_movementSpeed += m_escaltionSpeed;
		Vector3 newPosition = this.transform.position;

		newPosition.y -= m_movementEscalationDownMovement;

		this.transform.position = newPosition;
	}
	 

}
