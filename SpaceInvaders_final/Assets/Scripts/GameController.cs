﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	[SerializeField] float m_escalationInterval = 5.0f;

	[SerializeField] float m_enemyFireInterval = 2.0f;

	float m_lastEscalationTimeStamp;
	float m_lastEnemyFireTimeStamp;

	Object[] m_enemyObjects;

	PlayerActorController m_player;

	// Use this for initialization
	void Start () {

		m_enemyObjects = GameObject.FindObjectsOfType<EnemyActorController> ();

		m_player = (PlayerActorController) GameObject.FindObjectOfType<PlayerActorController> ();

	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown ("p")) {
		
			BaseActorController.m_isPaused = !BaseActorController.m_isPaused;
		}

		if (m_player == null || BaseActorController.m_isPaused) 
		{
			return;
		}

		if(m_lastEscalationTimeStamp < Time.time - m_escalationInterval) 
		{
			foreach(Object enemy in m_enemyObjects)
			{
				if (enemy) {
					((EnemyActorController)enemy).TriggerEscalate ();
				}
			}

			m_lastEscalationTimeStamp = Time.time;
		}


		if(m_lastEnemyFireTimeStamp < Time.time - m_enemyFireInterval) 
		{
			EnemyActorController nearestEnemy = null;
			float distanceToPlayer = float.PositiveInfinity;

			foreach(Object enemy in m_enemyObjects)
			{
				EnemyActorController enemyController = ((EnemyActorController)enemy);
				if (enemyController)
				{
					float enemyDistance = Vector3.Distance (m_player.transform.position, enemyController.transform.position);
					if(enemyDistance < distanceToPlayer)
					{
						distanceToPlayer = enemyDistance;
						nearestEnemy = enemyController;
					}
				}
			}

			if (nearestEnemy) 
			{
				Debug.Log ("FIRE PROJECTILE - " + nearestEnemy.name);
				nearestEnemy.FireProjectile ();
			}

			m_lastEnemyFireTimeStamp = Time.time;
		}
		
	}
}
