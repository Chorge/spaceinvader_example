﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : BaseActorController {

	[SerializeField] Vector3 m_directionVector;
	[SerializeField] float m_destructionDistance = 10.0f;

	override protected void ControlMovement()
	{
		transform.position += m_directionVector * Time.deltaTime;

		if (Vector3.Distance (transform.position, Vector3.zero) > m_destructionDistance) {
		
			Destroy (gameObject);
		
		}
	}
}
