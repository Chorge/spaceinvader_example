﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseActorController : MonoBehaviour {

	static public bool m_isPaused;
	
	// Update is called once per frame
	void Update () {
	
		if (!m_isPaused) {
			ControlMovement ();
		}

	}

	virtual protected void ControlMovement()
	{
	}
}
