﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatController : BaseActorController {

	[SerializeField] GameObject m_projectilePrefab;

	[SerializeField] Vector3 m_projectileOffset;

	public void FireProjectile()
	{
		GameObject.Instantiate (m_projectilePrefab,transform.position + m_projectileOffset, Quaternion.identity);
	}

	void OnCollisionEnter2D(Collision2D collide)
	{
		TriggerDestruction ();

		Destroy (collide.gameObject);
	}

	virtual protected void TriggerDestruction()
	{
		Destroy (gameObject);
	}
}
